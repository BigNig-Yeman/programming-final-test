import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author mahmo
 */
public class URLScanning {
    public static void main (String [] args){
    
        Scanner sc = new Scanner(System.in);
        
        int exit = 0;
        
        int safecount = 0;
        int comcount= 0;
        
        String safe = "https";
        String com = ".com";
        
        
        while(exit==0){
            
            System.out.println("Please enter an URL to scan.."
                    + "or 'stop' to, well, stop.");
            
            String url = sc.nextLine();
            String spacelessURL = url.replaceAll("\\s","");
            
            if(spacelessURL.length()>=6){
                String safetymark= spacelessURL.substring(0,6 );
                String commark= spacelessURL.substring(spacelessURL.length()-4);
            
                    if(safetymark.equalsIgnoreCase(safe) && commark.equalsIgnoreCase(com)){
            
                        safecount++;
                        comcount++;
                }else if(safetymark.equalsIgnoreCase(safe)){
                
                    safecount++;
                }else if(commark.equalsIgnoreCase(com)){
            
                    comcount++;
                
            }}else if(spacelessURL.equalsIgnoreCase("stop")){
                
                    exit++;
            }else{
            
                System.out.println("Please make sure to write valid URL!");
            }
            }
            
        System.out.println("***RESULTS***");
        System.out.println("Amount of safe URL: "+safecount);
        System.out.println("Amount of commercial URL: "+comcount);
                    
       
    }
    
}
