import java.util.Scanner;


/**
 *
 * @author mahmo
 */
public class URLScanning {
    public static void main (String [] args){
    
        Scanner sc = new Scanner(System.in);
        
        int exit = 0;
        
        int safecount = 0;
        int comcount= 0;
        
        String safe = "https";
        String com = ".com";
        
        System.out.println("Please enter the path of your file");
        String urlFile = sc.nextLine();
        
        File inFile = new File(urlFile);
        Scanner scFile = new Scanner(inFile);
        
        while(exit==0 && scFile.hasNext()){
            
            String url = sc.nextLine();
            String spacelessURL = url.replaceAll("\\s","");
            
            if(spacelessURL.length()>=6){
                String safetymark= spacelessURL.substring(0,6 );
                String commark= spacelessURL.substring(spacelessURL.length()-4);
            
                    if(safetymark.equalsIgnoreCase(safe) && commark.equalsIgnoreCase(com)){
            
                        safecount++;
                        comcount++;
                }else if(safetymark.equalsIgnoreCase(safe)){
                
                    safecount++;
                }else if(commark.equalsIgnoreCase(com)){
            
                    comcount++;
                
            }
            }else if(spacelessURL.equalsIgnoreCase("stop")){
                
                    exit++;
            }
            }
            
        System.out.println("***RESULTS***");
        System.out.println("Amount of safe URL: "+safecount);
        System.out.println("Amount of commercial URL: "+comcount);
                    
       
    }
    
}
